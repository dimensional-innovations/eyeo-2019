# Eyeo Festival 2019

Links, notes and resources from Eyeo Festival 2019.

- [Festival](http://eyeofestival.com/)
- [Speakers](http://eyeofestival.com/speakers/)
- [Past Talks](https://vimeo.com/eyeofestival)
- [Can Can Wonderland](https://photos.app.goo.gl/KimqeERuUge73sp29)

---

## [Adam Harvey](http://eyeofestival.com/speakers/adam-harvey/)

- [https://ahprojects.com/](https://ahprojects.com/)
- [https://megapixels.cc/](https://megapixels.cc/)

## [Chris Barr](http://eyeofestival.com/speakers/chris-barr/)

- [http://chrisbarr.net/](http://chrisbarr.net/)
- [https://knightfoundation.org/topics/arts-technology](https://knightfoundation.org/topics/arts-technology)
- [The Existential Void of the Pop-Up ‘Experience’](https://www.nytimes.com/2018/09/26/arts/color-factory-museum-of-ice-cream-rose-mansion-29rooms-candytopia.html)
- [The Museum of Ice Cream](https://www.museumoficecream.com/)

## [Daniel Shiffman](http://eyeofestival.com/speakers/daniel-shiffman/)

- [https://shiffman.net/](https://shiffman.net/)
- [https://www.youtube.com/user/shiffman](https://www.youtube.com/user/shiffman)
- [https://ml5js.org/](https://ml5js.org/)
- [https://p5js.org/](https://p5js.org/)
- [https://github.com/CodingTrain/Eyeo-Festival-2019](https://github.com/CodingTrain/Eyeo-Festival-2019)

## [Helena Sarin](http://eyeofestival.com/speakers/helena-sarin/)

- [https://www.neuralbricolage.com/](https://www.neuralbricolage.com/)
- [https://www.instagram.com/helena.sarin/](https://www.instagram.com/helena.sarin/)

## [Mario Klingemann](http://eyeofestival.com/speakers/mario-klingemann/)

- [http://quasimondo.com/](http://quasimondo.com/)
- [https://twitter.com/quasimondo](https://twitter.com/quasimondo)

## [Meow Wolf](http://eyeofestival.com/speakers/meow-wolf/)

- [http://meowwolf.com/](http://meowwolf.com/)
- [http://twitter.com/MeowWolf](http://twitter.com/MeowWolf)
- [https://www.youtube.com/watch?v=ve4CrNWaAXY](https://www.youtube.com/watch?v=ve4CrNWaAXY)

## [Mohit Bhoite](http://eyeofestival.com/speakers/mohit-bhoite/)

- [https://www.bhoite.com/](https://www.bhoite.com/)
- [https://www.particle.io/](https://www.particle.io/)
- [https://www.instagram.com/mohitbhoite/](https://www.instagram.com/mohitbhoite/)

## [Moritz Stefaner](http://eyeofestival.com/speakers/moritz-stefaner/)

- [http://truth-and-beauty.net/](http://truth-and-beauty.net/)
- [http://datastori.es/](http://datastori.es/)
- [http://data-cuisine.net/](http://data-cuisine.net/)

## [Onyx Ashanti](http://eyeofestival.com/speakers/onyx-ashanti/)

- [https://onyxashanti.bandcamp.com/album/node-0-initialization-suite-2013-2019](https://onyxashanti.bandcamp.com/album/node-0-initialization-suite-2013-2019)
- [https://www.youtube.com/user/onyxashanti](https://www.youtube.com/user/onyxashanti)

## [Refik Anadol](http://eyeofestival.com/speakers/refik-anadol/)

- [http://refikanadol.com/](http://refikanadol.com/)
- [https://www.charlotteobserver.com/entertainment/arts-culture/article214769365.html](https://www.charlotteobserver.com/entertainment/arts-culture/article214769365.html)
- [https://twitter.com/refikanadol](https://twitter.com/refikanadol)
